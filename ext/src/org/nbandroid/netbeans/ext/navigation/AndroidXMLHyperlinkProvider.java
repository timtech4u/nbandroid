package org.nbandroid.netbeans.ext.navigation;

import java.util.EnumSet;
import java.util.Set;
import javax.swing.text.Document;
import org.netbeans.lib.editor.hyperlink.spi.HyperlinkProviderExt;
import org.netbeans.lib.editor.hyperlink.spi.HyperlinkType;

/**
 * Hyperlink support to go to resource for from identifiers like R.layout._name_ or R.id._something_.
 */
public final class AndroidXMLHyperlinkProvider implements HyperlinkProviderExt {
  private final XmlGoToSupport goToSupport = new XmlGoToSupport(new UiUtilsCallerImpl(new OpenExecutorImpl()));

  public AndroidXMLHyperlinkProvider() {
  }

  @Override
  public Set<HyperlinkType> getSupportedHyperlinkTypes() {
    return EnumSet.of(HyperlinkType.GO_TO_DECLARATION);
  }

  @Override
  public boolean isHyperlinkPoint(Document doc, int offset, HyperlinkType type) {
    boolean is = getHyperlinkSpan(doc, offset, type) != null;
    return is;
  }

  @Override
  public int[] getHyperlinkSpan(Document doc, int offset, HyperlinkType type) {
    GoToContext refSpan = goToSupport.getIdentifierSpan(doc, offset);
    return refSpan != null ? new int[] { refSpan.offsetFrom, refSpan.offsetTo } : null;
  }

  @Override
  public void performClickAction(Document doc, int offset, HyperlinkType type) {
    switch (type) {
      case GO_TO_DECLARATION:
        goToSupport.goTo(doc, offset);
        break;
    }
  }

  @Override
  public String getTooltipText(Document doc, int offset, HyperlinkType type) {
    return goToSupport.getGoToElementTooltip(doc, offset);
  }
}
