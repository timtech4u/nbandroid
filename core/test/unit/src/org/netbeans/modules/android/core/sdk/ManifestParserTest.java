/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.netbeans.modules.android.core.sdk;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import java.io.File;
import java.util.List;
import org.junit.Test;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

import static org.junit.Assert.*;

public class ManifestParserTest  {

  private static final String SDK_DIR = System.getProperty("test.all.android.sdks.home");

  @Test
  public void projectThemes() throws Exception {
    ManifestParser p = new ManifestParser();
    Iterable<String> noThemes = p.getManifestThemeNames(null);
    assertTrue(Iterables.isEmpty(noThemes));
    
    FileObject manifestFo = FileUtil.toFileObject(new File(SDK_DIR, "samples/android-13/Home/AndroidManifest.xml"));
    List<String> themes = Lists.newArrayList(p.getManifestThemeNames(manifestFo.getInputStream()));
    assertEquals(1, themes.size());
    assertEquals("Theme", themes.get(0));
  }
  
  @Test
  public void projectThemesFromStylesXml() throws Exception {
    ManifestParser p = new ManifestParser();
    Iterable<String> noThemes = p.getProjectThemeNames(null);
    assertTrue(Iterables.isEmpty(noThemes));
    
    FileObject manifestFo = FileUtil.toFileObject(new File(SDK_DIR, "samples/android-15/ApiDemos/res/values/styles.xml"));
    List<String> themes = Lists.newArrayList(p.getProjectThemeNames(manifestFo.getInputStream()));
    assertEquals(16, themes.size());
    assertTrue(themes.contains("Theme"));
    assertTrue(themes.contains("CustomTheme"));
    assertTrue(themes.contains("BadTheme"));
    assertTrue(themes.contains("ImageView120dpi"));
  }
  
  @Test
  public void projectThemes2() throws Exception {
    ManifestParser p = new ManifestParser();
    
    FileObject manifestFo = FileUtil.toFileObject(new File(SDK_DIR, "samples/android-13/ApiDemos/AndroidManifest.xml"));
    List<String> themes = Lists.newArrayList(p.getManifestThemeNames(manifestFo.getInputStream()));
    assertEquals(8, themes.size());
    for (String t : new String[] {"CustomTheme", "Theme.Black", 
                                  "Theme.CustomDialog", "ThemeDialogWhenLarge", 
                                  "ThemeHolo", "Theme.Translucent", 
                                  "Theme.Transparent", 
                                  "Theme.Wallpaper"}) {
      assertTrue(themes.toString(), themes.contains(t));
    }
  }
  
  @Test
  public void appName() throws Exception {
    ManifestParser p = new ManifestParser();
    
    FileObject manifestFo = FileUtil.toFileObject(new File(SDK_DIR, "samples/android-13/ApiDemos/AndroidManifest.xml"));
    ManifestParser.ApplicationData data = p.getApplicationData(manifestFo.getInputStream());
    assertEquals("@string/activity_sample_code", data.appLabel);
  }
}
