package org.nbandroid.netbeans.gradle;

import org.netbeans.api.project.Project;
import org.netbeans.gradle.project.api.entry.GradleProjectExtension;
import org.netbeans.gradle.project.api.entry.GradleProjectExtensionQuery;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author radim
 */
@ServiceProvider(service = GradleProjectExtensionQuery.class, position = 900)
public class AndroidGradleProjectExtQuery implements GradleProjectExtensionQuery {

  @Override
  public GradleProjectExtension loadExtensionForProject(Project project) {
    AndroidGradleExtension ext = new AndroidGradleExtension(project);
    for (AndroidGradleCallback callback : Lookup.getDefault().lookupAll(AndroidGradleCallback.class)) {
      callback.customize(ext, project);
    }
    return ext;
  }
}
