/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.netbeans.modules.android.project;

import org.netbeans.modules.android.project.api.PropertyName;
import com.android.sdklib.internal.project.ProjectProperties;
import org.netbeans.spi.project.support.ant.PropertyUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.api.project.Project;
import org.netbeans.modules.android.core.sdk.DalvikPlatformManager;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import static org.junit.Assert.*;

/**
 * Tests for AndroidActionProvider
 */
public class PropertiesHelperTest {

  private static AndroidTestFixture fixture;

  private static FileObject projdir;
  private static Project pp;

  @BeforeClass
  public static void setUpClass() throws Exception {
    fixture = AndroidTestFixture.create().withProject("TicTacToeMain", "samples/android-8/TicTacToeMain");
    projdir = fixture.getProjectFolder("TicTacToeMain");
    pp = fixture.getProject("TicTacToeMain");
  }

  @AfterClass
  public static void delete() {
    fixture.tearDown();
  }

  @Test
  public void fixLocalProperties() {
    FileObject localProps = projdir.getFileObject("local.properties");
    assertNull("samples are shipped without local.properties", localProps);
    PropertiesHelper helper = new PropertiesHelper((AndroidProjectImpl) pp);
    helper.updateProperties(DalvikPlatformManager.getDefault());

    localProps = projdir.getFileObject("local.properties");
    assertNotNull("local.properties is created now", localProps);
    assertEquals(AndroidTestFixture.SDK_DIR, PropertyUtils.propertiesFilePropertyProvider(
            FileUtil.toFile(localProps)).getProperties().get("sdk.dir"));
  }

  @Test
  public void hasCommonProperties() {
    PropertiesHelper helper = new PropertiesHelper((AndroidProjectImpl) pp);
    assertEquals("TicTacToeMain", helper.evaluator().getProperty("project.name"));
    assertEquals("TicTacToeMain", helper.evaluator().getProperty("project.displayName"));

  }

  @Test
  public void doNotCreateEmptyFiles() {
    PropertiesHelper helper = new PropertiesHelper((AndroidProjectImpl) pp);
    assertNull(pp.getProjectDirectory().getFileObject("ant.properties"));
    helper.setProperty(ProjectProperties.PropertyType.ANT, PropertyName.JAR_LIBS_DIR.getName(), null);
    assertNull(pp.getProjectDirectory().getFileObject("ant.properties"));
  }
}
