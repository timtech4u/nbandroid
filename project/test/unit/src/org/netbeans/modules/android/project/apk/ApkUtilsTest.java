/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.netbeans.modules.android.project.apk;

import com.google.common.io.Files;
import com.google.common.io.Resources;
import java.io.File;
import java.io.IOException;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author radim
 */
public class ApkUtilsTest {
  @Test
  public void manifestParsing() throws IOException {
    File apk = File.createTempFile("spinner", ".apk");
    Resources
        .asByteSource(ApkUtilsTest.class.getResource("Spinner-12-debug-unaligned.apk"))
        .copyTo(Files.asByteSink(apk));
    
    ApkUtils decompresser = new ApkUtils();
    assertEquals("com.android.example.spinner", decompresser.apkPackageName(apk).getPackage());
    apk.delete();
  }
  
  @Test
  public void manifestParsingNull() throws IOException {
    ApkUtils decompresser = new ApkUtils();
    assertNull(decompresser.apkPackageName(null));
  }
}
